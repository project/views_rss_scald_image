CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module is an extension of the Views RSS module.

Natively, the Views RSS module supports the insertion of images in the
enclosure element of the RSS feed. But, if you use the Scald media library as
multimedia content manager, the image field would not be a simple image but
a Scald "atom reference" image.

This extension module adds the same functionality if the image is a Scald
atom reference image and not a simple image.

REQUIREMENTS
------------

This module requires the following modules:
 * Views RSS (https://www.drupal.org/project/views_rss)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

RECOMMENDED MODULES
-------------------

 * Scald: Media Management made easy (https://www.drupal.org/project/scald)

CONFIGURATION
-------------

You have to configure a view, a feed in the view and to configure the feed
first (detailed information in Views module and Views RSS module).

Once Views and Views RSS are functional, please follow these steps:

 * In the view configuration:
  * [Field part] Add the desired image field
  * [Field part] Click on the image element to configure the views field
  * [Formatter part] Select the "RSS enclosure element" formatter.

 * Associate your image to the RSS feed - Field:
  * [Format part] Click on parameters -> Item elements:Core -> select the
    image field for the enclosure item.

MAINTAINERS
-----------

Current maintainers:
 * Sébastien Prouff (SebBd) - https://www.drupal.org/user/3171535

This project has been sponsored by:
 * Smile - http://www.smile.fr
